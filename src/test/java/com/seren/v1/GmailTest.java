package com.seren.v1;

import com.codeborne.selenide.Configuration;
import com.seren.v1.pages.Mails;
import com.seren.v1.pages.Gmail;
import com.seren.v1.pages.Menu;
import com.seren.v1.testconfigs.BaseTest;
import com.seren.v1.testdata.Credentials;

import com.seren.v1.core.Helpers;
import org.junit.Test;


import static com.codeborne.selenide.Selenide.open;
/**
 * Created by Herold on 18.07.2016.
 */
public class GmailTest extends BaseTest {

    static {
        Configuration.timeout = 15000;
    }

    @Test
    public void loginSendAndSearchTest(){

        String subject = Helpers.getUniqueString("Subject");

        open("http://gmail.com");

        Gmail.loginAs(Credentials.email,Credentials.password);

        Mails.sendMail(Credentials.email, subject);

        Mails.refresh();
        Mails.assertMail(0,subject);

        Menu.goToSent();
        Mails.assertMail(0,subject);

        Menu.goToInbox();
        Mails.search(subject);
        Mails.assertMailTexts(subject);
    }
}
