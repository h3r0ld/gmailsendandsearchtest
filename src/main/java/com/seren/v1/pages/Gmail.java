package com.seren.v1.pages;

import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Herold on 26.07.2016.
 */
public class Gmail {
    @Step
    public static void loginAs(String login, String password) {
        //$("#gmail-sign-in").click();
        $("#Email").setValue(login).pressEnter();
        $("#Passwd").setValue(password).pressEnter();
    }
}
