package com.seren.v1.pages;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;


import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by Herold on 19.07.2016.
 */
public class Mails {
    static ElementsCollection emails = $$("[role='main'] .zA");

    @Step
    public static void sendMail(String email, String subject) {
        $(byText("НАПИСАТЬ")).click();
        $(By.name("to")).setValue(email);
        $(By.name("subjectbox")).setValue(subject);
        $(byText("Отправить")).click();
    }

    @Step
    public static void refresh(){
        $(".asf").click();
    }

    @Step
    public static void assertMail(int index, String mailText) {
        emails.get(index).shouldHave(text(mailText));
    }

    @Step
    public static void search(String subject){
        $(By.name("q")).setValue("subject:"+subject).pressEnter();
    }

    @Step
    public static void assertMailTexts(String ...mailTexts){
        emails.shouldHave(texts(mailTexts));
    }
}
