package com.seren.v1.pages;

import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selectors.byTitle;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Herold on 27.07.2016.
 */
public class Menu {

    @Step
    public static void goToInbox(){
        $(byTitle("Входящие")).click();
    }

    @Step
    public static void goToSent(){
        $(byTitle("Отправленные")).click();
    }
}
