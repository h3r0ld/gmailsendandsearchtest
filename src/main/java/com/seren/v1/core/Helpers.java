package com.seren.v1.core;


import java.time.LocalTime;

/**
 * Created by Herold on 21.07.2016.
 */
public class Helpers {

    public static String getUniqueString(String prefix) {
        return prefix +" "+  LocalTime.now().toString();
    }
}